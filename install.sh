#!/bin/bash

ncore=16
PWD=`pwd`
bnx_dir="$PWD/drivers/net/ethernet/broadcom/bnx2x"

date
make -j $ncore > make.out 2>&1
date
make modules -j $ncore >> make.out 2>&1
#make modules M=$bnx_dir -j $ncore > make_install.out 2>&1
#sudo make modules_install M=$bnx_dir -j $ncore >> make_install.out 2>&1
sudo make modules_install -j $ncore > make_install.out 2>&1
date
sudo make install -j $ncore >> make_install.out 2>&1
date
