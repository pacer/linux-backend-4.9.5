/*
 * sme_debug.h
 *
 * created on: Feb 21, 2017
 * author: aasthakm
 * 
 * debug macros
 */

#ifndef __SME_DEBUG_H__
#define __SME_DEBUG_H__

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/kern_levels.h>
#include <linux/sched.h>
#include <linux/printk.h>

#define SME_DEBUG_LVL 0

#define PFX "SME"

#define itrace	\
	if (SME_DEBUG_LVL >= 0) {	\
		printk(KERN_INFO PFX "[(%d) %s:%d]\n",	\
				task_tgid_nr(current), __func__, __LINE__);	\
	}

#define iprintk(LVL, F)	\
	if (SME_DEBUG_LVL >= LVL) {	\
		printk(KERN_INFO PFX "[(%d) %s:%d] " F "\n",	\
				task_tgid_nr(current), __func__, __LINE__);	\
	}

#define iprintk2(LVL, F)	\
	if (SME_DEBUG_LVL >= LVL) {	\
		printk(KERN_INFO F "\n");	\
	}

#define iprint(LVL, F, A...)	\
	if (SME_DEBUG_LVL >= LVL) {	\
		printk(KERN_INFO PFX "[(%d) %s:%d] " F "\n",	\
				task_tgid_nr(current), __func__, __LINE__, A);	\
	}

#define iprint2(LVL, F, A...)	\
	if (SME_DEBUG_LVL >= LVL) {	\
		printk(KERN_INFO "(%d) " F "\n", task_tgid_nr(current), A);	\
	}

#endif /* __SME_DEBUG_H__ */
