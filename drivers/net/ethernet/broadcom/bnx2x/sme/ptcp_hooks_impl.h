/*
 * ptcp_hooks_impl.h
 *
 * created on: Jun 21, 2018
 * author: aasthakm
 *
 * Similar to lsm_hooks.h
 * Provides LSM like generic interface to 
 * create pluggable function interfaces
 * 
 */

#ifndef __PTCP_HOOKS_IMPL_H__
#define __PTCP_HOOKS_IMPL_H__

#include <linux/skbuff.h>
#include <linux/tcp.h>

/*
 * Note: this does not implement the additional level 
 * of generic interfaces similar to the ones provided 
 * by security.c/h, which allow for multiple 
 * implementations of the LSM
 */

union ptcp_list_options {
  void (*print_sock_skb) (struct sock *sk, struct sk_buff *skb, char *dbg_str);
  int (*rx_pred_flags) (struct sock *sk, struct sk_buff *skb, struct tcphdr *th);
  int (*rx_handle_tcp_urg) (struct sock *sk, struct sk_buff *skb, struct tcphdr *th);
  int (*rx_handle_dummy) (struct sock *sk, struct sk_buff *skb);
  int (*rx_disable_coalesce) (struct sock *sk, struct sk_buff *to,
      struct sk_buff *from);
  int (*rx_adjust_skb_size) (struct sock *sk, struct sk_buff *skb, int req_len,
      int offset, int chunk, int copied, int flags);
  int (*rx_adjust_copied_seq) (struct sock *sk, struct sk_buff *skb,
      int old_skb_len, int new_skb_len);
  int (*tx_adjust_seq) (struct sock *sk, struct sk_buff *skb, int copy, int copied);
  int (*rxtx_sync_shared_seq) (struct sock *sk, struct sk_buff *skb, int write);
  int (*tx_adjust_urg) (struct sock *sk, struct sk_buff *skb);
  int (*tx_incr_tail) (struct sock *sk, struct sk_buff *skb);
  int (*tx_incr_profile) (struct sock *sk, struct sk_buff *skb);
  int (*is_paced_flow) (struct sock *sk);
  void (*timer_rto_check) (struct sock *sk);
  int (*get_retransmit_skb) (struct sock *sk, struct sk_buff **skb_p);
  int (*build_rexmit_dummy) (struct sock *sk, struct sk_buff **skb_p,
      uint32_t seqno, uint32_t seqack, int sacked_flags);
};

struct ptcp_hook_heads {
  struct list_head print_sock_skb;
  struct list_head rx_pred_flags;
  struct list_head rx_handle_tcp_urg;
  struct list_head rx_handle_dummy;
  struct list_head rx_disable_coalesce;
  struct list_head rx_adjust_skb_size;
  struct list_head rx_adjust_copied_seq;
  struct list_head tx_adjust_seq;
  struct list_head rxtx_sync_shared_seq;
  struct list_head tx_adjust_urg;
  struct list_head tx_incr_tail;
  struct list_head tx_incr_profile;
  struct list_head is_paced_flow;
  struct list_head timer_rto_check;
  struct list_head get_retransmit_skb;
  struct list_head build_rexmit_dummy;
};

struct ptcp_hook_list {
	struct list_head list;
	struct list_head *head;
	union ptcp_list_options hook;
};

#define PTCP_HOOK_INIT(HEAD, HOOK)	\
{ .head = &ptcp_hook_heads.HEAD, .hook = { . HEAD = HOOK } }

// increment this every time a new hook is added
#define NUM_PTCP_HOOKS  16

extern struct ptcp_hook_heads ptcp_hook_heads;
extern struct ptcp_hook_list ptcp_hooks[NUM_PTCP_HOOKS];

static inline void ptcp_add_hooks(struct ptcp_hook_list *hooks, int count)
{
	int i;
	for (i = 0; i < count; i++)
		list_add_tail_rcu(&hooks[i].list, hooks[i].head);
}

static inline void ptcp_delete_hooks(struct ptcp_hook_list *hooks, int count)
{
	int i;
	for (i = 0; i < count; i++)
		list_del_rcu(&hooks[i].list);
}

#endif /* __PTCP_HOOKS_IMPL_H__ */
