/*
 * xen_sme_hooks.h
 *
 * created on: Feb 25, 2017
 * author: aasthakm
 *
 * Similar to security.h/c
 * symbols exported to be used in the netback driver
 * 
 */

#ifndef __XEN_SME_HOOKS_H__
#define __XEN_SME_HOOKS_H__

//#include "../../../../xen-netback/common.h"
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include "../bnx2x.h"
#include "sme_debug.h"

enum {
  C_TCP_ACK = 0,
  C_TCP_ENTER_LOSS,
  C_TCP_UNDO_CWND_REDUCTION,
  C_TCP_END_CWND_REDUCTION,
  C_TCP_CWND_RESTART,
  C_TCP_CWND_APP_LIMITED,
};

void xsl_update_cwnd(struct sk_buff *skb, struct sock *sk, int caller,
    int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
    int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
    int una_diff, int delivered, int ack_flag, int num_marked_lost,
    char *extra_dbg_string);
int xsl_intercept_select_queue(struct net_device *dev, struct sk_buff *skb,
    char *extra_dbg_string);
void xsl_intercept_tx_sent_queue(struct netdev_queue *txq, unsigned int len);
void xsl_intercept_tx_completed_queue(struct netdev_queue *txq,
    unsigned int pkts, unsigned int bytes);
int xsl_intercept_sk_buff(struct sk_buff *skb, char *extra_dbg_string);
void xsl_intercept_xmit_stop_queue(struct net_device *dev, struct netdev_queue *txq,
    struct bnx2x_fp_txdata *txdata);
int xsl_intercept_tx_int(struct net_device *dev, struct bnx2x_fp_txdata *txdata);
void xsl_intercept_free_tx_skbs_queue(struct bnx2x_fastpath *fp);

void xsl_parse_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons, char *data,
		int data_len, char *extra_dbg_string);
void xsl_print_sk_buff(struct sk_buff *skb, struct sock *sk, char *extra_dbg_string);
void xsl_intercept_rx_path(void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons, int irq,
		char *extra_dbg_string);
void xsl_print_rx_data(void *dev, int fp_idx, uint16_t rx_bd_prod,
		uint16_t rx_bd_cons, uint16_t rx_comp_prod, uint16_t rx_comp_cons,
		char *extra_dbg_string);

#endif /* __XEN_SME_HOOKS_H__ */
