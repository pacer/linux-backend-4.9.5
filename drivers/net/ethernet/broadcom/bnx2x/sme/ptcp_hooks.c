/*
 * ptcp_hooks_impl.c
 *
 * created on: Jun 21, 2018
 * author: aasthakm
 *
 * Similar to security.h/c
 * symbols exported to be used in TCP/IP
 * 
 */

#include "ptcp_hooks.h"
#include "ptcp_hooks_impl.h"

#define call_ptcp_void_hook(FUNC, ...)	({	\
		do {	\
			struct ptcp_hook_list *P;	\
			list_for_each_entry(P, &ptcp_hook_heads.FUNC, list)	{	\
				P->hook.FUNC(__VA_ARGS__);	\
			}	\
		} while (0);	\
	})

#define call_ptcp_int_hook(FUNC, IRC, ...)	({	\
		int RC = IRC;	\
		do {	\
			struct ptcp_hook_list *P;	\
			list_for_each_entry(P, &ptcp_hook_heads.FUNC, list)	{	\
				RC = P->hook.FUNC(__VA_ARGS__);	\
				if (RC != 0)	\
					break;	\
			}	\
		} while (0);	\
		RC;	\
	})


int PTCP_MTU = DEFAULT_PTCP_MTU;
EXPORT_SYMBOL(PTCP_MTU);

struct ptcp_hook_heads ptcp_hook_heads = {
  .print_sock_skb = LIST_HEAD_INIT(ptcp_hook_heads.print_sock_skb),
  .rx_pred_flags = LIST_HEAD_INIT(ptcp_hook_heads.rx_pred_flags),
  .rx_handle_tcp_urg = LIST_HEAD_INIT(ptcp_hook_heads.rx_handle_tcp_urg),
  .rx_handle_dummy = LIST_HEAD_INIT(ptcp_hook_heads.rx_handle_dummy),
  .rx_disable_coalesce = LIST_HEAD_INIT(ptcp_hook_heads.rx_disable_coalesce),
  .rx_adjust_skb_size = LIST_HEAD_INIT(ptcp_hook_heads.rx_adjust_skb_size),
  .rx_adjust_copied_seq = LIST_HEAD_INIT(ptcp_hook_heads.rx_adjust_copied_seq),
  .tx_adjust_seq = LIST_HEAD_INIT(ptcp_hook_heads.tx_adjust_seq),
  .rxtx_sync_shared_seq = LIST_HEAD_INIT(ptcp_hook_heads.rxtx_sync_shared_seq),
  .tx_adjust_urg = LIST_HEAD_INIT(ptcp_hook_heads.tx_adjust_urg),
  .tx_incr_tail = LIST_HEAD_INIT(ptcp_hook_heads.tx_incr_tail),
  .tx_incr_profile = LIST_HEAD_INIT(ptcp_hook_heads.tx_incr_profile),
  .is_paced_flow = LIST_HEAD_INIT(ptcp_hook_heads.is_paced_flow),
  .timer_rto_check = LIST_HEAD_INIT(ptcp_hook_heads.timer_rto_check),
  .get_retransmit_skb = LIST_HEAD_INIT(ptcp_hook_heads.get_retransmit_skb),
  .build_rexmit_dummy = LIST_HEAD_INIT(ptcp_hook_heads.build_rexmit_dummy),
};

void ptcp_print_sock_skb(struct sock *sk, struct sk_buff *skb, char *dbg_str)
{
  return call_ptcp_void_hook(print_sock_skb, sk, skb, dbg_str);
}
EXPORT_SYMBOL(ptcp_print_sock_skb);

int ptcp_rx_pred_flags(struct sock *sk, struct sk_buff *skb, struct tcphdr *th)
{
  return call_ptcp_int_hook(rx_pred_flags, -1, sk, skb, th);
}
EXPORT_SYMBOL(ptcp_rx_pred_flags);

int ptcp_rx_handle_tcp_urg(struct sock *sk, struct sk_buff *skb, struct tcphdr *th)
{
  return call_ptcp_int_hook(rx_handle_tcp_urg, -1, sk, skb, th);
}
EXPORT_SYMBOL(ptcp_rx_handle_tcp_urg);

int ptcp_rx_handle_dummy(struct sock *sk, struct sk_buff *skb)
{
  return call_ptcp_int_hook(rx_handle_dummy, -1, sk, skb);
}
EXPORT_SYMBOL(ptcp_rx_handle_dummy);

int ptcp_rx_disable_coalesce(struct sock *sk, struct sk_buff *to,
    struct sk_buff *from)
{
  return call_ptcp_int_hook(rx_disable_coalesce, -1, sk, to, from);
}
EXPORT_SYMBOL(ptcp_rx_disable_coalesce);

int ptcp_rx_adjust_skb_size(struct sock *sk, struct sk_buff *skb, int req_len,
    int offset, int chunk, int copied, int flags)
{
  return call_ptcp_int_hook(rx_adjust_skb_size, -1, sk, skb, req_len, offset, chunk,
      copied, flags);
}
EXPORT_SYMBOL(ptcp_rx_adjust_skb_size);

int ptcp_rx_adjust_copied_seq(struct sock *sk, struct sk_buff *skb,
    int old_skb_len, int new_skb_len)
{
  return call_ptcp_int_hook(rx_adjust_copied_seq, -1, sk, skb, old_skb_len,
      new_skb_len);
}
EXPORT_SYMBOL(ptcp_rx_adjust_copied_seq);

int ptcp_tx_adjust_seq(struct sock *sk, struct sk_buff *skb, int copy, int copied)
{
  return call_ptcp_int_hook(tx_adjust_seq, -1, sk, skb, copy, copied);
}
EXPORT_SYMBOL(ptcp_tx_adjust_seq);

int ptcp_rxtx_sync_shared_seq(struct sock *sk, struct sk_buff *skb, int write)
{
  return call_ptcp_int_hook(rxtx_sync_shared_seq, -1, sk, skb, write);
}
EXPORT_SYMBOL(ptcp_rxtx_sync_shared_seq);

int ptcp_tx_adjust_urg(struct sock *sk, struct sk_buff *skb)
{
  return call_ptcp_int_hook(tx_adjust_urg, -1, sk, skb);
}
EXPORT_SYMBOL(ptcp_tx_adjust_urg);

int ptcp_tx_incr_tail(struct sock *sk, struct sk_buff *skb)
{
  return call_ptcp_int_hook(tx_incr_tail, -1, sk, skb);
}
EXPORT_SYMBOL(ptcp_tx_incr_tail);

int ptcp_tx_incr_profile(struct sock *sk, struct sk_buff *skb)
{
  return call_ptcp_int_hook(tx_incr_profile, -1, sk, skb);
}
EXPORT_SYMBOL(ptcp_tx_incr_profile);

int ptcp_is_paced_flow(struct sock *sk)
{
  return call_ptcp_int_hook(is_paced_flow, -1, sk);
}
EXPORT_SYMBOL(ptcp_is_paced_flow);

void ptcp_timer_rto_check(struct sock *sk)
{
  return call_ptcp_void_hook(timer_rto_check, sk);
}
EXPORT_SYMBOL(ptcp_timer_rto_check);

int ptcp_get_retransmit_skb(struct sock *sk, struct sk_buff **skb_p)
{
  return call_ptcp_int_hook(get_retransmit_skb, -1, sk, skb_p);
}
EXPORT_SYMBOL(ptcp_get_retransmit_skb);

int ptcp_build_rexmit_dummy(struct sock *sk, struct sk_buff **skb_p,
    uint32_t seqno, uint32_t seqack, int sacked_flags)
{
  return call_ptcp_int_hook(build_rexmit_dummy, -1, sk, skb_p,
      seqno, seqack, sacked_flags);
}
EXPORT_SYMBOL(ptcp_build_rexmit_dummy);
